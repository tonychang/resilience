### Resilience project outline
##### author:   Tony Chang and Kathryn Ireland

This repository serves as the store house for the landscape resilience assessment project. The central thesis of the research is to investigate a quantifiable metric of resilience for forested systems at the landscape scale and determine if there is a strong connection between resilience and response to disturbances. 

## Table of Content
 - [Introduction](#introduction)
 - [Objective](#objective)
 - [Methods](#methods)
 - [Anticipated Results](#results)
 - [Schedule](#schedule)
 - [References](#references)

## Introduction
Ecosystem resilience has been a central paradigm in ecology and land resource management since its introduction. This concept focuses on the assumption that if a ecosystem is highly resilient, then following a disturbance on the system, it will be able to recover back to its original starting state. However, definitions of resilience are vague and lack some type of quantifiable metric for researchers to compare different systems and determine some continuous spectrum of resilience. 

## Objective
The goal of this research is to define a metric of resilience and test if it meets the criteria of the conceptual definition based on the response of a disturbed simulated ecosystem using a Dynamic Global Vegetation Model.

## Methods
In conceptual definitions, systems that are thought of as resilience have high structural diversity and biodiversity. In a forest system, which we hope to use as a case study, that means a resilient system is one that has wide demographic distribution and high species richness. These quantities can be measured by standard metrics such [Shannon's Index (H)](https://www.fs.fed.us/pnw/pubs/pnw_gtr688/papers/Inv%20&%20Mon/session3/boehl.pdf) for structural diversity in terms of age, height, or diameter at breast height; and [Simpson's Diversity Index (D)](http://www.tiem.utk.edu/~gross/bioed/bealsmodules/simpsonDI.html). Then some combination of these two measures of diversity should predict the amount of time for the return to an original state given some measure of disturbance. 

### Example 1
single species and old aged (aspen) stand (170 years)

#### Treatment
 - low fire -> total change in function (hi), amount of time to return (170 years)
 - mid fire -> total change in function (hi), amount of time to return (170 years)
 - high fire -> total change in function (hi), amount of time to return (170 years)

### Example 2
multi-species and age structure stand (PICO, PIAL, ABLA, POTR) (ages range from 1-500 years)

#### Treatment
 - low fire -> total change in function (saplings killed in POTR and ABLA), amount of time to return (20 years) --> low change
 - mid fire -> total change in function (saplings killed of ALL, some killed of juvenilles for ALL, and some mature killed of ABLA and POTR), amount of time to return (100 years)
 - high fire -> total change in function (saplings, juvenilles, and mature killed of ALL), amount of time to return (500 years)


## Anticipated Results

## Schedule

## References
 
 
